---
title: "Communications Department"
date: 0001-01-01
summary: ""
tags: ["Team"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

- [Xinyi (Cindy) Zhang](https://www.facebook.com/cindyzhang99/), Technical Writer
- [Isabella  (Isabella ) Mckay ](https://linkedin.com/in/isabellamckay/), Technical Writer 
- Johnny (Johnny) Ye, Communications Associate
- **[Shubhra (Shubhra) Bedi](https://www.github.com/shubhra-bedi/), VP Communications**
- [Michal (Michal) Fishkin ](https://linkedin.com/in/michal-fishkin/), Technical Writer 
- Dorje (Dorje) Kongtsa, Technical Writer
- Xi Yan, Photographer
- [Robert (Rupert) Wu](https://leglesslamb.gitlab.io), Web Developer, Academics Associate
