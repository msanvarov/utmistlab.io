---
title: "External Department"
date: 0001-01-01
summary: ""
tags: ["Team"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

- **Shrey (Shrey) Jain, VP External**
- [Malhar (Malhar) Shah](https://linkedin.com/in/malharshah22/), External Associate
- Zizhao (Zoe) Chen, External Associate
- Yixuan (Richard) Xu, External Associate
- [Yuchen (Raina) Wang](https://www.github.com/yuchenWYC/), External Associate
- Lily Zhang, External Associate 
- Zitian Wu, External Associate 
- Shuyi  Wang, External Associate 
- Yueze (Rex) Fang, External Associate 
- Claire Dalkie, External Associate 
- Sophie Zhang, External Associate
- Yuyao (Alina) Wei, Event Director
- Teng (Teng) Tu, External Associate, Marketing Associate
