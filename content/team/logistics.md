---
title: "Logistics Department"
date: 0001-01-01
summary: ""
tags: ["Team"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

- **Chaojun (Vicky) Chen, VP Logistics**
- [Wei (Jenny) Wu](https://linkedin.com/in/jenny-wu-1641b811b/), Logistics Associate
