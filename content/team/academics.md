---
title: "Academics Department"
date: 0001-01-01
summary: ""
tags: ["Team"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

- [Zachary (Zach) Trefler](https://linkedin.com/in/zach-trefler/), Project Director
- **[Winnie (Winnie) Xu](https://linkedin.com/in/winnie-xu/), VP Academics**
- [Theodore (Theo) Wu](https://www.github.com/theowu23451/), Project Director
- [Matthieu (Matthieu) Chan Chee](https://www.github.com/dragionic/), Project Director
- [Abhishek (Abhishek) Moturu](https://www.twitter.com/abhi_saim/), Project Director
- [Wen Jie (Steven) Zhao](https://www.github.com/Swjzhao/), Project Associate
- Abhishek (Abhi) Mahadevan, Project Director
- **Jacob Kelly, VP Academics**
- Devansh Ranade, Project Associate
- Matthieu Chan Chee, Project Associate
