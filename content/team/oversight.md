---
title: "Oversight Department"
date: 0001-01-01
summary: ""
tags: ["Team"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

- **Hongyu (Charlie) Chen, President**
- Qiyang (Colin) Li, Scientific Advisor
- Sicong (Sheldon) Huang, Scientific Advisor
