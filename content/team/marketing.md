---
title: "Marketing Department"
date: 0001-01-01
summary: ""
tags: ["Team"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

- Teng (Teng) Tu, External Associate, Marketing Associate
- **[Siyan (Siyan) Zhao](https://www.github.com/siyanLearner/), VP Marketing**
- Ruiyao Li, Marketing Associate
- Lisa Li, Graphics Designer
