---
title: About Us
date: 0001-01-01
sidebar: true
sidebarlogo: whiteside
---

![AGM 2019 Photo](/images/agm2019exec.png)

---

The [University of Toronto](https://utoronto.ca), one of the top education institutes in the world, leads the way in the field of Artificial/Machine Intelligence (AI). It's home to [Geoffrey Hinton’s Machine Learning Group](http://www.cs.toronto.edu/~hinton/) – the first of the three world-leading forces in the field of deep learning. However, undergraduate students seldom have the opportunity of getting in touch with the scholars from the [Machine Learning Group](http://learning.cs.toronto.edu), or getting exposure to leading technology in AI. This creates a disconnect between undergraduate students and top scholars in AI. UTMIST aims to clear the mist by demystifying AI technology, which is the most trendy technology nowadays. As a student organization focused on AI research, UTMIST is to establish a close connection between the undergraduate students and top academic resources at U of T.

---

University of Toronto Machine Intelligence Student Team (UTMIST) is an officially certified student organization within the [University of Toronto](https://utoronto.ca). Our mission is to let more people get to know about AI, “Clear the mist”!
