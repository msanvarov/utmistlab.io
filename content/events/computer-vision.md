---
title: "Computer Vision"
date: 2019-02-05
summary: "This workshop covers basic knowledge of recent advances in computer vision. We’ll introduce the commonly used tools and models in solving computer vision tasks and explore how the frontier computer vision research is accelerating integration to real-life scenarios. Following that, we will demo a typical workflow for CV projects."
tags: ["Event","MIST101"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![Computer Vision](https://drive.google.com/u/0/uc?id=14EBnu_cJDMBmdEOAUo6RgAXtrdwNVH-0)

This workshop covers basic knowledge of recent advances in computer vision. We’ll introduce the commonly used tools and models in solving computer vision tasks and explore how the frontier computer vision research is accelerating integration to real-life scenarios. Following that, we will demo a typical workflow for CV projects.
---
Date/Time: **Tue, Feb 05 2019, 18:30.**

Location: **[Sidney Smith Hall](http://map.utoronto.ca/utsg/building/033) SS 2102.**
