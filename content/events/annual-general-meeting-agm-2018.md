---
title: "Annual General Meeting (AGM) 2018"
date: 2018-09-13
summary: "UTMIST’s mission is to clear the mist around Machine Learning and create a platform of opportunities for UofT students to get involved in the ML community in Toronto. This year we are hoping to bond with more AI/ML enthusiasts, no matter with previous experience or not. MIST101 will stay with a re-structured format for new-comers while more hands-on projects and deeper study with researchers on state-to-art techniques are provided."
tags: ["Event","Meeting"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![Annual General Meeting (AGM) 2018](https://drive.google.com/u/0/uc?id=14gpyI5FR71T76t3viPMSJetKMTOncUCK)

UTMIST’s mission is to clear the mist around Machine Learning and create a platform of opportunities for UofT students to get involved in the ML community in Toronto. This year we are hoping to bond with more AI/ML enthusiasts, no matter with previous experience or not. MIST101 will stay with a re-structured format for new-comers while more hands-on projects and deeper study with researchers on state-to-art techniques are provided.
---
Date/Time: **Thu, Sep 13 2018, 18:30.**
