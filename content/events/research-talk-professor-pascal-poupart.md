---
title: "Research Talk - Professor Pascal Poupart"
date: 2019-11-12
summary: " “Unsupervised Video Object Segmentation for Deep Reinforcement Learning”, by Professor Pascal Poupart. He will discuss a new technique for deep reinforcement learning that exploits flow film information, detects moving objects and uses their information for action selection."
tags: ["Event","Talk Series"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![Research Talk - Professor Pascal Poupart](https://drive.google.com/u/0/uc?id=1z0iCc9LAygLdAj3zmvT1N8EGlk4oZYuL)

 “Unsupervised Video Object Segmentation for Deep Reinforcement Learning”, by Professor Pascal Poupart. He will discuss a new technique for deep reinforcement learning that exploits flow film information, detects moving objects and uses their information for action selection.
---
Date/Time: **Tue, Nov 12 2019, 18:00.**

Location: **[Galbraith Building](http://map.utoronto.ca/utsg/building/070) GB 303.**
