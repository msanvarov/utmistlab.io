---
title: "Introduction to Machine Learning"
date: 2018-11-08
summary: "A general introduction to machine learning fundamentals and a demonstration of typical workflow in solving machine learning problems. The typical workflow is demonstrated here: https://youtu.be/4flbCsGBicE"
tags: ["Event","MIST101"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![Introduction to Machine Learning](https://drive.google.com/u/0/uc?id=1DdrIBgyt1f0aWGY_hytbIZQ7tcqNagN2)

A general introduction to machine learning fundamentals and a demonstration of typical workflow in solving machine learning problems. The typical workflow is demonstrated here: https://youtu.be/4flbCsGBicE
---
Date/Time: **Thu, Nov 08 2018, 18:30.**
