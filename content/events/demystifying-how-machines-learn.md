---
title: "Demystifying How Machines Learn"
date: 2020-01-28
summary: "The series will introduce the methodology of human psychology insights to solve unsupervised learning problems and build novel machine learning and mathematical models. The series will catalyze audience at all levels, especially those who are passionate about unsupervised learning and reinforcement learning.
Our speaker, Nishkrit Desai, will lead this series. He is a student researcher involved in projects with organizations such as Google, Tesla, Autodesk, and OpenAI. His main interests are unsupervised learning and reinforcement learning.
Join us in uncovering the exciting ideas in machine learning!
"
tags: ["Event","Talk Series"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![Demystifying How Machines Learn](https://drive.google.com/u/0/uc?id=1ofK_dJz-UH8rNe70xPTZFPkl2u6xubpa)

The series will introduce the methodology of human psychology insights to solve unsupervised learning problems and build novel machine learning and mathematical models. The series will catalyze audience at all levels, especially those who are passionate about unsupervised learning and reinforcement learning.
Our speaker, Nishkrit Desai, will lead this series. He is a student researcher involved in projects with organizations such as Google, Tesla, Autodesk, and OpenAI. His main interests are unsupervised learning and reinforcement learning.
Join us in uncovering the exciting ideas in machine learning!

---
Date/Time: **Tue, Jan 28 2020, 17:00.**

Location: **[Galbraith Building](http://map.utoronto.ca/utsg/building/070) GB 220.**
