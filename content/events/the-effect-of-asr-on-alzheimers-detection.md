---
title: "The Effect of ASR on Alzheimer's Detection"
date: 2019-03-05
summary: "Ever wondered how Machine Learning works for medical diagnostics? If so, don’t miss this insightful guest speaker talk given by Jekaterina Novikova. We are honoured to invite her to talk about the effect of ASR (automatic speech recognition) on classification performance in Alzheimer's detection.

Jekaterina is a director of Machine Learning at Winterlight Labs, where they focus on developing a novel AI technology that can quickly and accurately quantify speech and language patterns to help detect and monitor cognitive and mental diseases. Previously, she received her PhD from University of Bath. She also worked as a post-doctoral researcher at Heriot-Watt University in Edinburgh, UK, at the Interaction Lab and Natural Language Processing Lab.

Join us on March 5th for this insightful speaker session and learn from this excellent machine learning researcher!"
tags: ["Event","Talk Series"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![The Effect of ASR on Alzheimer's Detection](https://drive.google.com/u/0/uc?id=1pIzt9_T_moOBums_H3qXz7MCQ1Pq1KVL)

Ever wondered how Machine Learning works for medical diagnostics? If so, don’t miss this insightful guest speaker talk given by Jekaterina Novikova. We are honoured to invite her to talk about the effect of ASR (automatic speech recognition) on classification performance in Alzheimer's detection.

Jekaterina is a director of Machine Learning at Winterlight Labs, where they focus on developing a novel AI technology that can quickly and accurately quantify speech and language patterns to help detect and monitor cognitive and mental diseases. Previously, she received her PhD from University of Bath. She also worked as a post-doctoral researcher at Heriot-Watt University in Edinburgh, UK, at the Interaction Lab and Natural Language Processing Lab.

Join us on March 5th for this insightful speaker session and learn from this excellent machine learning researcher!
---
Date/Time: **Tue, Mar 05 2019, 18:30.**

Location: **[Bahen Centre for IT](http://map.utoronto.ca/utsg/building/080) BA 1130.**
