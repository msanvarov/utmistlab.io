---
title: "Deep Learning on Graphs: CNN, RNN, GNN"
date: 2018-10-09
summary: "We are hosting the first session of our research paper reading group series on Graph Neural Net on Computer Vision Problems with Arie Huan Ling from NVIDIA AI Research lab and Vector Institute as our guest speaker!"
tags: ["Event","Paper Reading"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![Deep Learning on Graphs: CNN, RNN, GNN](https://drive.google.com/u/0/uc?id=1u2I1s0vELbVRq888-IObR5DAc1NbGNaZ)

We are hosting the first session of our research paper reading group series on Graph Neural Net on Computer Vision Problems with Arie Huan Ling from NVIDIA AI Research lab and Vector Institute as our guest speaker!
---
Date/Time: **Tue, Oct 09 2018, 18:30.**

Location: **[Galbraith Building](http://map.utoronto.ca/utsg/building/070) GB 220.**
