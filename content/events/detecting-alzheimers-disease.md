---
title: "Detecting Alzheimer's Disease"
date: 2018-11-20
summary: "We had the honour to invite Jekaterina Novikova to talk about Machine Learning Methods in Detecting Alzheimer's Disease from Speech and Language. Jekaterina is a Director of Machine Learning at Winterlight Labs, where they focus on developing a novel AI technology that can quickly and accurately quantify speech and language patterns to help detect and monitor cognitive and mental diseases."
tags: ["Event","Talk Series"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![Detecting Alzheimer's Disease](https://drive.google.com/u/0/uc?id=1T50cSBLmvFrnX1R96WUIa2Wl_Vk1yS0t)

We had the honour to invite Jekaterina Novikova to talk about Machine Learning Methods in Detecting Alzheimer's Disease from Speech and Language. Jekaterina is a Director of Machine Learning at Winterlight Labs, where they focus on developing a novel AI technology that can quickly and accurately quantify speech and language patterns to help detect and monitor cognitive and mental diseases.
---
Date/Time: **Tue, Nov 20 2018, 18:30.**

Location: **[Sidney Smith Hall](http://map.utoronto.ca/utsg/building/033) SS 1074.**
