---
title: "Annual General Meeting (AGM) 2019"
date: 2019-09-16
summary: "
Come and meet the new executive team! We will be talking about a variety of events that will be held throughout the year. At the end of the AGM, project leads will also introduce new academic projects. They will meet with anyone interested in joining the project teams afterwards. Anyone applied/interested in applying for a project associate position is strongly encouraged to come and potentially find your team. "
tags: ["Event","Meeting"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![Annual General Meeting (AGM) 2019](https://drive.google.com/u/0/uc?id=1fM0QovTlXhJFWAtJm3JG-qXiRio25tqO)


Come and meet the new executive team! We will be talking about a variety of events that will be held throughout the year. At the end of the AGM, project leads will also introduce new academic projects. They will meet with anyone interested in joining the project teams afterwards. Anyone applied/interested in applying for a project associate position is strongly encouraged to come and potentially find your team. 
---
Date/Time: **Mon, Sep 16 2019, 18:00.**

Location: **[Bahen Centre for IT](http://map.utoronto.ca/utsg/building/080) BA 1170.**
