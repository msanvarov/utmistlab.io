---
title: "Reinforcement Learning"
date: 2018-11-19
summary: "Lunjun Zhang will be providing an enriched introduction to reinforcement learning, one of the most active fields of AI research today. The workshop will cover both the fundamental ideas and several state-of-the-art methods including value iteration, policy gradients, actor critic, Q learning, and exploration strategies. The workshop will also explore how concepts from optimisation and information theory are used for control."
tags: ["Event","MIST101"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![Reinforcement Learning](https://drive.google.com/u/0/uc?id=1vLkYN0xR9y7KH0dOcyTmAuB_gXC3JrKa)

Lunjun Zhang will be providing an enriched introduction to reinforcement learning, one of the most active fields of AI research today. The workshop will cover both the fundamental ideas and several state-of-the-art methods including value iteration, policy gradients, actor critic, Q learning, and exploration strategies. The workshop will also explore how concepts from optimisation and information theory are used for control.
---
Date/Time: **Mon, Nov 19 2018, 18:30.**

Location: **[Galbraith Building](http://map.utoronto.ca/utsg/building/070) GB 220.**
