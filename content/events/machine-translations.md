---
title: "Machine Translations"
date: 2018-10-30
summary: "We had the honour to invite Jun Gao to talk about Recent Advances in Neural Machine Translation. Jun Gao graduated from Peking University with fruitful experiences gained from working in both academia and industry. Currently, he is a graduate student in the Machine Learning Group here at UofT."
tags: ["Event","Talk Series"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![Machine Translations](https://drive.google.com/u/0/uc?id=1dUgMXjcXdL5ysxonERq7KtSbVjJpMa82)

We had the honour to invite Jun Gao to talk about Recent Advances in Neural Machine Translation. Jun Gao graduated from Peking University with fruitful experiences gained from working in both academia and industry. Currently, he is a graduate student in the Machine Learning Group here at UofT.
---
Date/Time: **Tue, Oct 30 2018, 18:30.**

Location: **[Earth Sciences Centre](http://map.utoronto.ca/utsg/building/062) ES B149.**
