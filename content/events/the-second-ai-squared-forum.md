---
title: "The Second AI-Squared Forum"
date: 2019-09-22
summary: "AI Squared is a forum for facilitating dialogues across boundaries in the AI industry. It is a great opportunity to
- Discuss big ideas with AI researchers working on cutting-edge projects
- Network with recruiters from AI companies such as Uber ATG and Shopify to secure internship opportunities
- Hear about the experience of undergraduates working at institutes such as the Vector Institute, Nvidia and Google
- Connect with AI professors and professionals from academia and industry"
tags: ["Event","Forum"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![The Second AI-Squared Forum](https://drive.google.com/u/0/uc?id=1efcEk4QMw4oePSEnYH8L_T-OZPF795hQ)

AI Squared is a forum for facilitating dialogues across boundaries in the AI industry. It is a great opportunity to
- Discuss big ideas with AI researchers working on cutting-edge projects
- Network with recruiters from AI companies such as Uber ATG and Shopify to secure internship opportunities
- Hear about the experience of undergraduates working at institutes such as the Vector Institute, Nvidia and Google
- Connect with AI professors and professionals from academia and industry
---
Date/Time: **Sun, Sep 22 2019, 08:30.**

Location: **[Bahen Centre for IT](http://map.utoronto.ca/utsg/building/080) BA Atrium.**
