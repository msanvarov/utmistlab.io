---
title: "Exploration in Reinforcement Learning"
date: 2019-04-25
summary: "Sicong (Sheldon) Huang is currently an undergrad at U of T. His research interests include machine learning and cognitive science. Currently he is a research intern at Vector Institute and Borealis AI, where he works on evaluating generative models. Previously he worked on deep generative models for domain transfer on text and music, such as TimbreTron (ICLR2019) and CipherGAN (ICLR2018). Currently he is interested in some fundamental questions regarding learning and cognition. He also co-founded two organizations in his free time including UTMIST and FOR.ai, an international distributed AI research collaboration.
He will be giving a broad introduction on exploration strategies in Reinforcement Learning. Exploration vs. exploitation trade-off is one of the core problem in modern deep reinforcement learning especially when the environment is complex and partially observable. Exploiting the best actions based on the current policy can yield a very good short-term return, but can easily get the agent stuck in a local minimum. The goal of this paper reading group is to demystify the principle behind the deep RL exploration problem. The topics of this paper reading group range from optimistic exploration, posterior sampling to information gain in exploration. In addition to those traditional approaches, Sheldon will also be going over several state-of-the-art approaches in the field."
tags: ["Event","Paper Reading"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![Exploration in Reinforcement Learning](https://drive.google.com/u/0/uc?id=1LxvQOnFPTSWfN104C4O0li-QM8lA-OGV)

Sicong (Sheldon) Huang is currently an undergrad at U of T. His research interests include machine learning and cognitive science. Currently he is a research intern at Vector Institute and Borealis AI, where he works on evaluating generative models. Previously he worked on deep generative models for domain transfer on text and music, such as TimbreTron (ICLR2019) and CipherGAN (ICLR2018). Currently he is interested in some fundamental questions regarding learning and cognition. He also co-founded two organizations in his free time including UTMIST and FOR.ai, an international distributed AI research collaboration.
He will be giving a broad introduction on exploration strategies in Reinforcement Learning. Exploration vs. exploitation trade-off is one of the core problem in modern deep reinforcement learning especially when the environment is complex and partially observable. Exploiting the best actions based on the current policy can yield a very good short-term return, but can easily get the agent stuck in a local minimum. The goal of this paper reading group is to demystify the principle behind the deep RL exploration problem. The topics of this paper reading group range from optimistic exploration, posterior sampling to information gain in exploration. In addition to those traditional approaches, Sheldon will also be going over several state-of-the-art approaches in the field.
---
Date/Time: **Thu, Apr 25 2019, 18:30.**
