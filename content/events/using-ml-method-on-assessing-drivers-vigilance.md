---
title: "Using ML Method on Assessing Drivers' Vigilance"
date: 2019-03-12
summary: "Speaker: Min Liang
Min is currently working as a data scientist at RBC after receiving her master’s degree in electrical engineering from McGill University. She has had fruitful research experience in ECE from Tianjin University, Harvard University, MITACS and McGill University.
This talk session will be featuring her MITACS research project at Alcohol Countermeasure System Corp., an alcohol tester design company, in 2017. The overall objective is to reliably assess drivers’ vigilance using non-intrusive measures. The data used for this project are collected by Smart Eye Pro, an eye-tracking system. The research project mainly focuses on identifying the most important features and building classification/regression systems to reliably detect drowsiness and fatigue.
Join us on March 12th. Get inspired, make connections and learn new skills from this talk session!"
tags: ["Event","Talk Series"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![Using ML Method on Assessing Drivers' Vigilance](https://drive.google.com/u/0/uc?id=1p-Evp_xp0JSvx57iUhwApfjR6ebH-_a3)

Speaker: Min Liang
Min is currently working as a data scientist at RBC after receiving her master’s degree in electrical engineering from McGill University. She has had fruitful research experience in ECE from Tianjin University, Harvard University, MITACS and McGill University.
This talk session will be featuring her MITACS research project at Alcohol Countermeasure System Corp., an alcohol tester design company, in 2017. The overall objective is to reliably assess drivers’ vigilance using non-intrusive measures. The data used for this project are collected by Smart Eye Pro, an eye-tracking system. The research project mainly focuses on identifying the most important features and building classification/regression systems to reliably detect drowsiness and fatigue.
Join us on March 12th. Get inspired, make connections and learn new skills from this talk session!
---
Date/Time: **Tue, Mar 12 2019, 18:30.**

Location: **[Bahen Centre for IT](http://map.utoronto.ca/utsg/building/080) BA 1180.**
