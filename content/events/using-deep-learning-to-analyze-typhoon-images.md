---
title: "Using Deep Learning to Analyze Typhoon Images"
date: 2018-10-23
summary: "Speaker: Danlan Chen
Machine Learning Researcher at Borealis AI, the machine learning research department at RBC
Master’s from McGill University, where she did research in Machine Learning in the Reasoning & Learning Lab under the supervision of Professor Doino Precup.
Check out her personal website: https://danlanchen.github.io/"
tags: ["Event","Paper Reading"]
hideLastModified: true
sidebar: true
sidebarlogo: whiteside
---

![Using Deep Learning to Analyze Typhoon Images](https://drive.google.com/u/0/uc?id=1ofWnNQ3LZHlgQnJatACQULdw-KmckOZn)

Speaker: Danlan Chen
Machine Learning Researcher at Borealis AI, the machine learning research department at RBC
Master’s from McGill University, where she did research in Machine Learning in the Reasoning & Learning Lab under the supervision of Professor Doino Precup.
Check out her personal website: https://danlanchen.github.io/
---
Date/Time: **Tue, Oct 23 2018, 18:30.**
