---
title: Our Team
date: 0001-01-02
sidebar: true
sidebarlogo: whiteside
---

### **President**

- Hongyu (Charlie) Chen

### **Departments**

- [Academics](academics)
- [Communications](communications)
- [External](external)
- [Finance](finance)
- [Logistics](logistics)
- [Marketing](marketing)
- [Oversight](oversight)

### **Recruiting**

Interested in joining us? [Click Here!](https://docs.google.com/forms/d/e/1FAIpQLSd56SHFgH2x0wpgts03Y_E0LlFuP5vDlv8ZUJcXo9pZlcging/viewform)
