---
title: Events
date: 0001-01-04
sidebar: true
sidebarlogo: whiteside
---
We regularly host events, on our own or in collaboration with other organizations.

>|Event|>|Date|>|Time|>|Location|
>|-----|-|----|-|----|-|--------|
>|[Demystifying How Machines Learn](demystifying-how-machines-learn)||Tue, Jan 28 2020,|| 17:00||[Galbraith Building](http://map.utoronto.ca/utsg/building/070) GB 220|
>|[Research Talk - Professor Pascal Poupart](research-talk-professor-pascal-poupart)||Tue, Nov 12 2019,|| 18:00||[Galbraith Building](http://map.utoronto.ca/utsg/building/070) GB 303|
>|[The Second AI-Squared Forum](the-second-ai-squared-forum)||Sun, Sep 22 2019,|| 08:30||[Bahen Centre for IT](http://map.utoronto.ca/utsg/building/080) BA Atrium|
>|[Annual General Meeting (AGM) 2019](annual-general-meeting-agm-2019)||Mon, Sep 16 2019,|| 18:00||[Bahen Centre for IT](http://map.utoronto.ca/utsg/building/080) BA 1170|
>|[Exploration in Reinforcement Learning](exploration-in-reinforcement-learning)||Thu, Apr 25 2019,|| 18:30|||
>|[Using ML Method on Assessing Drivers' Vigilance](using-ml-method-on-assessing-drivers-vigilance)||Tue, Mar 12 2019,|| 18:30||[Bahen Centre for IT](http://map.utoronto.ca/utsg/building/080) BA 1180|
>|[The Effect of ASR on Alzheimer's Detection](the-effect-of-asr-on-alzheimers-detection)||Tue, Mar 05 2019,|| 18:30||[Bahen Centre for IT](http://map.utoronto.ca/utsg/building/080) BA 1130|
>|[Computer Vision](computer-vision)||Tue, Feb 05 2019,|| 18:30||[Sidney Smith Hall](http://map.utoronto.ca/utsg/building/033) SS 2102|
>|[Machine Learning for Recommender Systems](machine-learning-for-recommender-systems)||Tue, Nov 27 2018,|| 18:30||[Sidney Smith Hall](http://map.utoronto.ca/utsg/building/033) SS 1074|
>|[Detecting Alzheimer's Disease](detecting-alzheimers-disease)||Tue, Nov 20 2018,|| 18:30||[Sidney Smith Hall](http://map.utoronto.ca/utsg/building/033) SS 1074|
>|[Reinforcement Learning](reinforcement-learning)||Mon, Nov 19 2018,|| 18:30||[Galbraith Building](http://map.utoronto.ca/utsg/building/070) GB 220|
>|[Introduction to Machine Learning](introduction-to-machine-learning)||Thu, Nov 08 2018,|| 18:30|||
>|[Machine Translations](machine-translations)||Tue, Oct 30 2018,|| 18:30||[Earth Sciences Centre](http://map.utoronto.ca/utsg/building/062) ES B149|
>|[Using Deep Learning to Analyze Typhoon Images](using-deep-learning-to-analyze-typhoon-images)||Tue, Oct 23 2018,|| 18:30|||
>|[Deep Learning on Graphs: CNN, RNN, GNN](deep-learning-on-graphs-cnn-rnn-gnn)||Tue, Oct 09 2018,|| 18:30||[Galbraith Building](http://map.utoronto.ca/utsg/building/070) GB 220|
>|[Annual General Meeting (AGM) 2018](annual-general-meeting-agm-2018)||Thu, Sep 13 2018,|| 18:30|||
