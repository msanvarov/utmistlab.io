module gitlab.com/utmist/utmist.gitlab.io

go 1.13

require (
	github.com/joho/godotenv v1.3.0
	golang.org/x/net v0.0.0-20200225223329-5d076fcf07a8
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	google.golang.org/api v0.19.0
)
